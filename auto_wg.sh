#!/bin/vbash

remote_host=$1
wg_interface=$2

# VyOS-specific catch to make sure we're the right group
if [ "$(id -g -n)" != 'vyattacfg' ] ; then
    exec sg vyattacfg -c "/bin/vbash $(readlink -f $0) $@"
fi

# VyOS-specific sourcing
source /opt/vyatta/etc/functions/script-template

# main script
newIP=$(getent hosts $remote_host | awk '{ print $1 }')
oldIP=$(run show interfaces wireguard $wg_interface endpoints | awk '{ print $2 }' | awk -F ':' '{ print $1 }')

echo "Resolved IP: ${newIP}"
echo "Old IP: ${oldIP}"

if [ $newIP != $oldIP ]; then
    echo "IPs don't match! Editing peer configuration."
    configure
    set interfaces wireguard $wg_interface peer location1 address $newIP
    commit
    save
fi

# final exit
exit
