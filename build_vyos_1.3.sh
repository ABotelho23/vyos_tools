#!/bin/bash

# install git
yum install -y git

# ensure build directory exists
mkdir -p /mnt/build/artifacts/vyos

# clean up old build dir
rm -rf /mnt/build/vyos/vyos-build

# change to build location
pushd /mnt/build/vyos

# clone repo
git clone -b equuleus --single-branch https://github.com/vyos/vyos-build

# pull image
podman pull docker.io/vyos/vyos-build:equuleus

# build
podman run --tty \
--volume "$(pwd)":/vyos \
--workdir /vyos/vyos-build \
--privileged \
--sysctl net.ipv6.conf.lo.disable_ipv6=0 \
-e GOSU_UID=$(id -u) -e GOSU_GID=$(id -g) \
docker.io/vyos/vyos-build:equuleus /bin/bash -c "./configure --architecture amd64 --build-by "botelho2305@gmail.com"; sudo make iso"

# copy image
vyos_date=$(date '+%Y-%m-%d')
mv vyos-build/build/live-image-amd64.hybrid.iso /mnt/build/artifacts/vyos/vyos-1.3-${vyos_date}-amd64.iso
chmod 555 /mnt/build/artifacts/vyos/*

# leave directory
popd
